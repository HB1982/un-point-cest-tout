<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
function dd($var){
    var_dump($var);
    die;
}
$app->tpl = (new Slim\Views\PhpRenderer('themes/'));
/**********************ROUTES GET**********************/
$app->get('/optimize[/{dir}/{file}]', function (Request $request, Response $response, $args) use ($app) {
    ((new Admin())->debugCompressing($args['dir'], $args['file'], "optimize")) ? $response->getBody()->write("Votre image a bien été optimisé") : "";
    return $response;
});
$app->get('/unoptimize[/{dir}/{file}]', function (Request $request, Response $response, $args) use ($app) {
    ((new Admin())->debugCompressing($args['dir'], $args['file'], "unptimize")) ? $response->getBody()->write("image d'origine récupérer") : "";
    return $response;
});
$app->get('/resetPassword/{key}', function (Request $request, Response $response, $args) use ($app) {
    return $app->tpl->render($response, "resetPassword.php", $args = ["key" => $args['key']]);
});
$app->get('/[{url}[/{id}[/{titre}]]]', function (Request $request, Response $response, $args) use ($app) {

    if ($args['url'] == "projet" && (int)@$args['id'] > 0) {
        $app->project = (new Project())->getBy(['active' => 1, 'id' => @$args['id']])[0];
        $app->category = (new Category())->getBy([ 'id' => @$args['id']])[0];
    }
    if ($args['url'] == "categorie" && (int)@$args['id'] > 0) {
        $app->category = (new Category())->getBy([ 'id' => @$args['id']])[0];
    }
    if ($args['url'] == "client" && (int)@$args['id'] > 0) {
        $app->client = (new Client())->getBy([ 'id' => @$args['id']])[0];
    }
    return $app->tpl->render($response, "page.php", [(new Pagebuilder())->getPageByURL($args['url']), $app]);
});
$app->post('/resetPassword[/{key}]', function (Request $request, Response $response, $args) use ($app) {
    if (!empty($args['key'])) {
        (new Auth())->resetPassword($args['key'], $_POST);
    } else {
        (new Auth())->ActiveResetPassword($_POST);
    }
    Tools::redirect('/');
});
$app->post('/editLANG', function (Request $request, Response $response, $args) use ($app) {
    @session_start();
    $_SESSION['lang'] = @$_POST['lang'];
    return $response;
});
$app->post('/contact', function (Request $request, Response $response, $args) use ($app) {
    $Mailer = new Mail();
    $Mailer->setSubject("Demande de contact");
    $Mailer->setBody("<li>Nom : " . @$_POST['nom'] . "</li><li>Téléphone : " . @$_POST['telephone'] . "</li><li>Email : " . @$_POST['email'] . "</li><li>Message : " . @$_POST['message'] . "</li>");
    if ($Mailer->send()['success']) {
        Tools::setFlash("success", 'Merci ! Nous vous répondrons dès que possible !');
    } else {
        Tools::setFlash("error", 'Désolé Votre message n\'a pas été envoyée  !');
    }
    Tools::redirect('/contact');

});
