$(document).on('resize', function () {
    $grid = $('.grid').isotope({
        // options
        itemSelector: '.grid-item',
    });
});


$grid = $('.grid').isotope({
    // options
    itemSelector: '.grid-item',
    masonry: {}
});
var $grid = $('.grid').isotope({
    // options
});
// filter items on button click
$('.container').on('click', 'button,span', function (e) {
    e.preventDefault();
    var filterValue = $(this).attr('data-filter');
    $grid.isotope({filter: filterValue});

});


$("img.lazyload").lazyload();
$(document).ready(function () {
    $(".grid-item .content-categ span").click(function () {
        document.location.href = $(this).attr("data-href");
    });
});
$(".button.btn-play, .popup .close").on("click", function (e) {
    e.preventDefault();
    $("#popup1").toggleClass('active');
});