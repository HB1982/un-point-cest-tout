<!doctype html>
<html lang="<?= LANG ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= WEBSITE_NAME ?> - <?= @$data[0]['PageName'] ?></title>
    <link rel="icon" type="image/png" sizes="32x32" href="<?= ASSETS_DIR ?>favicon.png">

    <script src="https://code.jquery.com/jquery-3.6.0.js"
            integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" href="<?=ASSETS_DIR ?>/css/style.css">
</head>
<body>
<?php include("parts/header.php"); ?>
<div id="<?= Tools::slug_file(@$data[0]['PageName']) ?>">
    <?php
    if (!empty($data[0]['block'])) {
        foreach ($data[0]['block'] as $el) {
            echo (new Blockbuilder())->generateBlock($el->id_block, $el->datas, Tools::slug_file(@$data[0]['PageName']),  $data[1], $data[0]);
        }
    }
    if ($data[0]["page"]) {
        include("blocks/" . $data[0]["page"] . ".php");
    }
    ?>
</div>
<?php
include("parts/footer.php");
/**if (DEBUG_TPNG == TRUE) {
include('parts/debug.php');}**/ ?>
</body>
</html>
