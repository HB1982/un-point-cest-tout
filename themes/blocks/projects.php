<div class="container">

    <?php
    $Categories = new Category();
    $list_categories = $Categories->getAll();
    ?>

    <nav class="button-group filter-button-group nav ">
        <?php foreach ($list_categories as $category) { ?>
            <div class="center">

                <a href="/categorie/<?= $category->id ?>" style="background-color: <?= $category->couleur ?>"
                ><?= substr($category->nom, 0, 1) ?></a>
                <p style="color: <?= $category->couleur ?>"> <?= $category->nom ?></p>
            </div>
        <?php } ?>
    </nav>

    <?php
    /** tools slugfiles*/
    $Produits = new Project();
    $list_produits = $Produits->getAll();
    $Categories = new Category(); ?>
    <div class="grid">
        <?php foreach ($list_produits as $produit) { ?>
            <a class="grid-item <?= $produit->size ?> <?= Tools::slug_file($produit->category) ?>"
               href="/projet/<?= $produit->id ?>">
                <div class="content-categ">
                    <?php foreach (json_decode($produit->id_category) as $key => $id_category) {
                        $category = $Categories->get($id_category);
                        ?>
                        <span style="background-color: <?= $category->couleur ?>"
                              data-href="/categorie/<?= $category->id ?>"><?= substr($category->nom, 0, 1) ?></span>
                    <?php } ?></div>
                <img class="lazyload"
                     src="" data-src="<?= Tools::generateThumbnail('https://youmatter.world/app/uploads/sites/3/2016/06/statistiques-oceans-developpement-durable.jpg', 400) ?>" alt="">
            </a>
        <?php } ?>
    </div>
s
    <?php
    if (isset ($_GET["category"])) { ?>
        <script>
            setTimeout(function () {
                let category = "<?= $_GET['category'] ?>";
                $('button[data-filter=".' + category + '"]').click();
            }, 500);
        </script>
        <?php
    }
    ?>
</div>
