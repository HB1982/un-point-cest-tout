<?php
$Clients = new Client();
$client = $Clients->get($app->project->id_user);
$Category = new Category();

?>
<div class="container">
    <?php
    $video = $app->project->video;
    if (!empty($video)) {
        ?>
        <a class="button btn-play" href="#popup1"><img class="play" src="/themes/assets/images/play.png" alt="lecture"></a>
        <div id="popup1" class="ovl">
            <div class="popup">
                <?= $video ?>
                <a class="close" href="#">&times;</a>
            </div>
        </div>
        <?php
    }
    ?>

    <img class="full" src="<?= $app->project->illustration ?>" alt="">
    <div class="content-single">
        <?php foreach (json_decode($app->project->id_category) as $key => $id_category) {
            $category = $Category->get($id_category);
            ?>
            <span class='spanproject'
                  style="background-color: <?= $category->couleur ?>"><?= substr($category->nom, 0, 1) ?></span>
        <?php } ?></div>


    <div class="profil">
        <img class="clientimg" src="<?= $client->illustration ?>" alt="imgclient">
        <a href="/client/<?= $client->id ?>""><?= $client->nom ?></a>
    </div>

    <p><?= $app->project->description ?></p>

    <?php foreach (json_decode($app->project->id_category) as $key => $id_category) {
        $projet = new Category();
        $list_projets = $projet->getProducts($id_category);
        foreach ($list_projets as $projets) { ?>
            <img class="grid-item lazyload <?= $projets->size ?>"
                 data-src="<?= Tools::generateThumbnail($projets->illustration, 400) ?>" alt="">
        <?php }
    }
    ?>


</div>
