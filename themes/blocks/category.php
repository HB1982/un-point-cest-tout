<div class="container">
    <img class="full" src="<?= $app->category->illustration ?>" alt="">
    <h1><?= $app->category->nom ?></h1>
    <p><?= $app->category->description ?></p>

    <?php
    $Categories = new Category();
    $projetsByCateg = $Categories->getProducts($app->category->id);
    ?>

    <div class="grid">
        <?php foreach ($projetsByCateg as $produit) { ?>
            <a class="grid-item <?= $produit->size ?> <?= Tools::slug_file($produit->category) ?>"
               href="/projet/<?= $produit->id ?>">
                <div class="content-categ">
                    <?php foreach (json_decode($produit->id_category) as $key => $id_category) {
                        $category = $Categories->get($id_category);
                        ?>
                        <span style="background-color: <?= $category->couleur ?>"
                              data-href="/categorie/<?= $category->id ?>"><?= substr($category->nom, 0, 1) ?></span>
                    <?php } ?></div>
                <img class="lazyload"
                     src="" data-src="<?= Tools::generateThumbnail($produit->illustration, 400) ?>" alt="">
            </a>
        <?php } ?>
    </div>


</div>
