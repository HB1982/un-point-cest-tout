<div class="container">
    <img class="full" src="<?= $app->client->illustration ?>" alt="">
    <p><?= $app->client->nom ?></p>


    <?php
    $projets = new Project();
    $projet = new Client();
    $Categories = new Category();
    $list_projets = $projet->getProducts($app->client->id);
    ?>

    <div class="grid">
        <?php foreach ($list_projets as $produit) { ?>
            <a class="grid-item <?= $produit->size ?> <?= Tools::slug_file($produit->category) ?>"
               href="/projet/<?= $produit->id ?>">
                <div class="content-categ">
                    <?php foreach (json_decode($produit->id_category) as $key => $id_category) {
                        $category = $Categories->get($id_category);
                        ?>
                        <span style="background-color: <?= $category->couleur ?>"
                              data-href="/categorie/<?= $category->id ?>"><?= substr($category->nom, 0, 1) ?></span>
                    <?php } ?></div>
                <img class="lazyload"
                     src="" data-src="<?= Tools::generateThumbnail($produit->illustration, 400) ?>" alt="">
            </a>
        <?php } ?>
    </div>
</div>
