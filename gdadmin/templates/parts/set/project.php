<?php $el = @$data['datas']; ?>
<div class="card col-10 offset-1">
    <div class="card-header card-header-tabs card-header-primary">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <span class="nav-tabs-title">Article</span>
                <ul class="nav nav-tabs float-right">
                    <?php if (@$_GET['trad'] == "en") { ?>
                        <li class="nav-item mr-3"><a class="nav-link active" href="?trad=fr"><i
                                        class="fas fa-globe"></i> Français</a></li>
                    <?php } else { ?>
                        <li class="nav-item mr-3"><a class="nav-link active" href="?trad=en"><i
                                        class="fas fa-globe"></i> Anglais</a></li>
                    <?php } ?>
                    <li class="nav-item mr-3"><a class="nav-link active" href="javascript:history.go(-1)"><i
                                    class="fas fa-arrow-left"></i> Retour</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane active">
                <form class="row" method="post" enctype="multipart/form-data">


                    <?php
                    $Clients = new Client();
                    $tab = [];
                    $Categories = new Category();

                    foreach ($Clients->getAll() as $client) $tab[$client->id] = $client->nom;


                    ?>

                    <?= Tools::generateSelect("choisissez votre client", "id_user", $tab, @$el->id_user, "form-select col-12"); ?>
                    <div class="col-12">Choisissez votre catégorie</div>
                    <?php foreach ($Categories->getAll() as $categorie) { ?>
                        <label for="<?= $categorie->nom ?>"><?= $categorie->nom ?></label>
                        <input type="checkbox" name="id_category[]" value="<?= $categorie->id ?>">
                    <?php } ?>
                    <?= Tools::generateInput("date", "Date", "date", @$el->date, "col-12"); ?>
                    <?= Tools::generateInput("text", "description", "description", @$el->{"description" . _PREFIX_LANG_}, "col-6"); ?>
                    <?= Tools::generateInput("file", "Illustration", "illustration", @$el->illustration, 'col-12'); ?>
                    <?= Tools::generateInput("text", "video", "video", @$el->video, 'col-12'); ?>
                    <?= Tools::generateInput("radio", "type de taille", "size", @$el->size, 'col-12', '', ['standard' => "", 'double hauteur' => "grid-item--height2", 'double largeur' => 'grid-item--width2', 'grand format' => 'grid-item--height2 grid-item--width2']); ?>
                    <?= Tools::generateInput("hidden", "", "id", @$el->id); ?>
                    <?= Tools::generateInput("radio", "Publié", "active", @$el->active, 'col-12', '', ['oui' => 1, 'non' => 0]); ?>
                    <?= Tools::generateInput("submit", "", "", "Valider", "btn-style2 float-right"); ?>
                </form>
            </div>
        </div>
    </div
</div>
